using backend.Domain;

namespace backend.Application;

public interface ICountryService
{
    public Country CreateCountry(Country country);
    
    public bool CountryExists(Guid id);
    
    public Country DeleteCountry(Guid id);
    
    public Country UpdateCountry(Guid id, Country country);
    
    public IEnumerable<Country> GetAllCountries();
    
    public Country GetCountryById(Guid id);
}