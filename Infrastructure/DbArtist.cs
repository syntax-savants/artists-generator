namespace backend.Infrastructure;

public class DbArtist : DbEntity
{
    public DbArtist(Guid id) : base(id)
    {
    }
    
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateTime StartDate { get; set; }
}