using AutoMapper;
using backend.Domain;
using backend.Domain.models;
using Microsoft.EntityFrameworkCore;

namespace backend.Infrastructure;

public class DbRepository<TEntity, TDbEntity> : IRepository<TEntity, Guid>
    where TEntity : Entity<Guid>
    where TDbEntity : DbEntity
{

    public DbRepository(MyAppContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    protected MyAppContext context { get; }

    protected IMapper mapper { get; }

    public IEnumerable<TEntity> GetAll()
    {
        var dbEntities = this.context.Set<TDbEntity>();
        var domainEntities = this.mapper.Map<IEnumerable<TDbEntity>, IEnumerable<TEntity>>(dbEntities);
        return domainEntities;
    }

    public TEntity Create(TEntity entity)
    {
        var dbEntity = this.mapper.Map<TEntity, TDbEntity>(entity);
        dbEntity.Id = Guid.NewGuid();

        var savedEntity = this.context.Set<TDbEntity>().Add(dbEntity).Entity;
        this.context.SaveChanges();
        var domainEntity = this.mapper.Map<TDbEntity, TEntity>(savedEntity);
        return domainEntity;
    }

    public bool Exists(Guid id)
    {
        return this.Exists<TDbEntity>(id);
    }

    public bool Exists<T>(Guid id) where T : DbEntity
    {
        return this.context.Set<T>().Any(e => e.Id == id);
    }

    private TDbEntity GetDbEntityById(Guid id)
    {
        var dbEntity = this.context.Set<TDbEntity>().AsNoTracking().FirstOrDefault(e => e.Id == id);
        if (dbEntity == null)
        {
            throw new Exception($"Entity '{id}' not found.");
        }
        return dbEntity;
    }

    public TEntity GetById(Guid id)
    {
        var dbEntity = this.GetDbEntityById(id);
        var domainEntity = this.mapper.Map<TDbEntity, TEntity>(dbEntity);
        return domainEntity;
    }

    public TEntity Delete(Guid id)
    {
        var dbEntity = this.GetDbEntityById(id);
        var domainEntity = this.mapper.Map<TDbEntity, TEntity>(dbEntity);
        this.context.Set<TDbEntity>().Remove(dbEntity);
        this.context.SaveChanges();
        return domainEntity;
    }

    public TEntity Update(TEntity entity)
    {
        TDbEntity newDbEntity = this.mapper.Map<TEntity, TDbEntity>(entity);
        this.context.Update(newDbEntity);
        this.context.SaveChanges();
        return this.GetById(entity.Id);
    }
}
