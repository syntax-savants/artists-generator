using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace backend.Infrastructure;

public class DbArtistTypeConfiguration : IEntityTypeConfiguration<DbArtist>
{
    public void Configure(EntityTypeBuilder<DbArtist> builder)
    {
        builder.ToTable("Artists");
        builder.HasKey(x => x.Id);
        builder.Property(x => x.FirstName).IsRequired();
        builder.Property(x => x.LastName).IsRequired();
        builder.Property(x => x.StartDate).IsRequired();
    }
}