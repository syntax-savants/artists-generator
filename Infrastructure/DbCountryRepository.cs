using AutoMapper;
using backend.Domain;

namespace backend.Infrastructure;

public class DbCountryRepository : DbRepository<Country, DbCountry>, ICountryRepository
{
    public DbCountryRepository(MyAppContext context, IMapper mapper) : base(context, mapper)
    {
    }
}