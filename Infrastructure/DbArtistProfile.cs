using AutoMapper;
using backend.Domain;

namespace backend.Infrastructure;

public class DbArtistProfile : Profile
{
    public DbArtistProfile()
    {
        this.CreateMap<DbArtist, Artist>().ConvertUsing(a => new Artist(a.Id)
        {
            FirstName = a.FirstName,
            LastName = a.LastName,
            StartDate = DateOnly.FromDateTime(a.StartDate)
        });
        
        this.CreateMap<Artist, DbArtist>().ConvertUsing(a => new DbArtist(a.Id)
        {
            FirstName = a.FirstName,
            LastName = a.LastName,
            StartDate = a.StartDate.ToDateTime(new TimeOnly())
        });
    }
}