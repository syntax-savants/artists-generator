using Microsoft.EntityFrameworkCore;

namespace backend.Infrastructure;

public class MyAppContext : DbContext
{
    public MyAppContext(DbContextOptions<MyAppContext> options) : base(options)
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MyAppContext).Assembly);
        base.OnModelCreating(modelBuilder);
    }
}