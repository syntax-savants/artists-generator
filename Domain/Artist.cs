using backend.Domain.models;

namespace backend.Domain;

public class Artist : Entity<Guid>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public DateOnly StartDate { get; set; }

    public Artist(Guid id) : base(id)
    {

    }
}