using backend.Domain.models;

namespace backend.Domain;

public class Country : Entity<Guid>
{
    public string Name { get; set; }
    
    public string Capital { get; set; }

    public int Population { get; set; }

    public Country(Guid id) : base(id)
    {
    }
}